-Empezando el programa: al comienzo deberemos introducir un numero, luego nos preguntara si queremos seguir introduciendo numeros, si ponemos una s, seguiremos introduciendo numeros, cuando colocamos una n, el programa se finalizara y nos mostrara dos lista una con los numeros positivos que introducimos y otra lista con numeros negativos, osea que nuestra primera lista la dividimos en dos lista distinas.

-Instalacion: Para la instalacion de este pograma debemos ingresar a este repositorio https://gitlab.com/bryan433/guia3-1algoritmo.git clonarlo, colocando en la terminal, git clone y el url del repositorio, luego de eso debemos entrar a la carpeta donde tenemos este archivo ingresar donde estan todos los ficheros abrir la terminal desde ahi, colocar make para compilar el programa y quedar nuestro programa fuente del que se va inicializar todo nuestro pograma, al finalizar el make, deberemos colocar este comando ./programa y se podra iniciar.

-Sistema operativo:
Debian GNU/Linux 10 (buster)

-Autor:
Bryan Ahumada Ibarra
