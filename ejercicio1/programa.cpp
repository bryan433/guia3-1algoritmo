#include <iostream>
#include <stdlib.h>
#include "Lista.h"

using namespace std;

void dividir_lista(Lista *&lista, Lista *&negativa, Lista *&positiva){
	int pos;
	int neg;
	Nodo *tmp = new Nodo();
	tmp = lista->get_raiz();
	while (tmp != NULL){
		if (tmp->numero < 0){
			neg = tmp->numero;
			negativa->crear(neg);
			negativa->Ordenar_lista();
		}
		else{
			pos = tmp->numero;
			positiva->crear(pos);
			positiva->Ordenar_lista();
		}
		tmp = tmp->sig;
	}
	cout << "\n";
	cout << "Mostrando lista de numeros positivos\n";
	positiva->imprimir();
	cout << "\n";
	cout << "Mostrando lista de numeros negativos\n";
	negativa->imprimir(); 
	

}




int main (void){
	Lista *lista = new Lista();
	Lista *positiva =  new Lista();
	Lista *negativa = new Lista();
	int numero;
	char opcion = 's';
	
	while(opcion == 's'){
		
		cout << "inserte un numero entero a su lista enlazada 1: " << endl;
		cin >> numero;
		
		/* creamos nuestro nodo */ 
		lista->crear(numero);
		lista->Ordenar_lista();
		
		cout << "¿Desea seguir igresando numeros? s(si) / n(no)" << endl;
		cin >> opcion;
		system("clear");
	}
	dividir_lista(*&lista, *&negativa, *&positiva);
}
