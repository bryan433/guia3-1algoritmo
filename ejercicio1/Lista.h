#include <iostream>
#include "programa.h"

using namespace std;

#ifndef LISTA_H
#define LISTA_H

class Lista {
    private:
        Nodo *raiz = NULL;
        Nodo *ultimo = NULL;

    public:
        /* constructor*/
        Lista();
        
        /* crea un nuevo nodo, recibe una instancia de la clase Persona. */
        void crear (int n);
        /* Ordenamos nuesta lista enlazada */
        void Ordenar_lista();
        /* imprime la lista. */
        void imprimir ();
        Nodo* get_raiz();
};
#endif
	
