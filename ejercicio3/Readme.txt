-empezando el programa: al comenzar nos saldra un menu de 5 opciones,la opcion 1 nos permitira agregar el nombre a nuestra receta, la opcion 2 nos mostrara la lista de recetas que tenemos, la opcion 3 dejara buscar una receta, de la cual cuando la encontramos, nos mostrara otro menu el cual, consta de 4 opciones, la opcion 1 nos mostrara los ingredientes(si esta vacia no nos mostrara nada), la opcion 2 nos dejara añadir ingredientes a la receta, la opcion 3 nos dejara eliminar algun ingrediente, y la opcion 4 nos dejara salir, al volver al primer menu del programa la opcion 4 nos dejaria eliminar alguna receta,
y la ultima opcion que es la 5 nos dejara cerrar el programa.

-Instalacion: Para la instalacion de este pograma debemos ingresar a este repositorio https://gitlab.com/bryan433/guia3-1algoritmo.git clonarlo, colocando en la terminal, git clone y el url del repositorio, luego de eso debemos entrar a la carpeta donde tenemos este archivo ingresar donde estan todos los ficheros abrir la terminal desde ahi, colocar make para compilar el programa y quedar nuestro programa fuente del que se va inicializar todo nuestro pograma, al finalizar el make, deberemos colocar este comando ./programa y se podra iniciar.

-Sistema operativo:
Debian GNU/Linux 10 (buster)

-Autor:
Bryan Ahumada Ibarra	
