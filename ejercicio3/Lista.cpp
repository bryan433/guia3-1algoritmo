#include <iostream>
#include <stdlib.h>
#include <string.h>
#include "Lista.h"

using namespace std;

Lista::Lista(){
	
}

void Lista::crear (string n) {
    /* crea un nodo . */
    Nodo *tmp = new Nodo;
	
	tmp->dato = n;
    /* apunta a NULL por defecto. */
    tmp->sig = NULL;

    /* si el es primer nodo de la lista, lo deja como raíz y como último nodo. */
    if (this->raiz == NULL) { 
        this->raiz = tmp;
        this->ultimo = this->raiz;
    /* de lo contrario, apunta el actual último nodo al nuevo y deja el nuevo como el último de la lista. */
    } else {
        this->ultimo->sig = tmp;
        this->ultimo = tmp;
    }
}


void Lista::imprimir(){
	
	Nodo *tmp = this->raiz;
	
	while (tmp != NULL){
		cout << tmp->dato << endl;
		tmp = tmp->sig; 
	}
	cout << "\n";	
}

Nodo* Lista::get_raiz(){
	return this->raiz;
}


