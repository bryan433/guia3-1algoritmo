#include <iostream>
#include <stdlib.h>
#include <string>

using namespace std;

/* define la estructura del nodo. */
typedef struct _Nodo {
    string dato;
    struct _Nodo *sig;
} Nodo;
