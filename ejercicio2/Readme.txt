-Empezando el programa: al comenzar nos pedira colocar el nombre, despues debemos apretar el enter, luego de terminar de ingresar el nombre, se nos imprimira por pantalla el nombre, luego de esto nos preguntara si queremos seguir introduciendo nombres, si ponemos que no, se finalizara, si colocamos que si seguiremos introduciendo nombres.

-Instalacion: Para la instalacion de este pograma debemos ingresar a este repositorio https://gitlab.com/bryan433/guia3-1algoritmo.git clonarlo, colocando en la terminal, git clone y el url del repositorio, luego de eso debemos entrar a la carpeta donde tenemos este archivo ingresar donde estan todos los ficheros abrir la terminal desde ahi, colocar make para compilar el programa y quedar nuestro programa fuente del que se va inicializar todo nuestro pograma, al finalizar el make, deberemos colocar este comando ./programa y se podra iniciar.

-Sistema operativo:
Debian GNU/Linux 10 (buster)

-Autor:
Bryan Ahumada Ibarra
